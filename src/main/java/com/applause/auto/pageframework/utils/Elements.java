package com.applause.auto.pageframework.utils;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Elements
{
	public static void waitForElementToDisplay(AppiumDriver driver, WebElement elementToBeWaitedFor)
	{
		WebDriverWait wait = new WebDriverWait(driver, 60000, 100);
		wait.until(ExpectedConditions.elementToBeClickable(elementToBeWaitedFor));
	}
}
