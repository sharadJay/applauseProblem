package com.applause.auto.pageframework.utils;

import org.openqa.selenium.Dimension;

import io.appium.java_client.AppiumDriver;

public class Actions
{
	public static void swipe(AppiumDriver driver, double startX, double startY, double endX, double endY, int timeDuration)
	{
		Dimension dimension = driver.manage()
									.window()
									.getSize();
		int horizontal = dimension.getWidth();
		int vertical = dimension.getHeight();
		
		int instartX = (int) (startX * horizontal);
		int instartY = (int) (startY * vertical);
		int inendX = (int) (endX * horizontal);
		int inendY = (int) (endY * vertical);
		System.out.println(instartX + "," + instartY + "," + inendX + "," + inendY);
		driver.swipe(instartX, instartY, inendX, inendY, timeDuration);
	}
}
