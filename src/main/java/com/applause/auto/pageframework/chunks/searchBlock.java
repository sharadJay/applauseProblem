package com.applause.auto.pageframework.chunks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class searchBlock
{
	private AppiumDriver driver;
	private WebElement parentElement;
	private String blockTitle;
	private WebElement rating;
	private int numberOfReviews;
	
	public searchBlock(AppiumDriver driver, WebElement parentElement)
	{
		this.driver = driver;
		blockTitle = parentElement.findElement(By.id("com.wholefoods.wholefoodsmarket:id/searchRecipesItemTitle"))
									.getText();
	}
	
	public int getNumberOfReviews()
	{
		this.numberOfReviews = Integer.valueOf(parentElement.findElement(By.id("com.wholefoods.wholefoodsmarket:id/recipesItemRatingText"))
															.getText());
		return numberOfReviews;
	}
	
	public String getTitle()
	{
		return blockTitle;
	}
	
	// TODO return object of screen that should be returned
	public Object click()
	{
		parentElement.click();
		return null;
	}
	
}