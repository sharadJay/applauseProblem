package com.applause.auto.pageframework.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.applause.auto.pageframework.utils.Elements;

public class homeScreen
{
	private AppiumDriver driver;
	
	@AndroidFindBy(id = "com.wholefoods.wholefoodsmarket:id/etHomeSearch")
	private WebElement searchInput;
	
	@AndroidFindBy(id = "com.wholefoods.wholefoodsmarket:id/recipeSearchClearBtn")
	private WebElement searchClearCrossButton;
	
	@AndroidFindBy(id = "com.wholefoods.wholefoodsmarket:id/imgSearch")
	private WebElement searchButton;
	
	public homeScreen(AppiumDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		Elements.waitForElementToDisplay(driver, searchInput);
	}
	
	public homeScreen enterSearchString(String searchKeyword)
	{
		searchInput.sendKeys(searchKeyword);
		Elements.waitForElementToDisplay(driver, searchClearCrossButton);
		return this;
	}
	
	public searchResultsScreen clickOnSearchButton()
	{
		searchButton.click();
		System.out.println("Clicked on search button");
		return new searchResultsScreen(driver);
		
	}
	
}
