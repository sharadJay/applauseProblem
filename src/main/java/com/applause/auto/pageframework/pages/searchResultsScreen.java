package com.applause.auto.pageframework.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.applause.auto.pageframework.chunks.searchBlock;
import com.applause.auto.pageframework.utils.Actions;
import com.applause.auto.pageframework.utils.Elements;

public class searchResultsScreen
{
	HashMap<String, searchBlock> searchBlocksList;
	
	private AppiumDriver driver;
	
	@AndroidFindBy(id = "com.wholefoods.wholefoodsmarket:id/searchResultsNumber")
	private WebElement searchResultsNumber;
	
	public searchResultsScreen(AppiumDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		Elements.waitForElementToDisplay(driver, searchResultsNumber);
		setListOfSearches();
	}
	
	public Integer getResultsNumberDisplayed()
	{
		String resultText = searchResultsNumber.getText();
		System.out.println("Results are " + resultText);
		return Integer.parseInt(resultText);
	}
	
	private HashMap<String, searchBlock> setListOfSearches()
	{
		int expectedCount = getResultsNumberDisplayed();
		searchBlocksList = new HashMap<String, searchBlock>();
		do
		{
			List<WebElement> listOfSearch = driver.findElements(By.xpath("//*[@resource-id='com.wholefoods.wholefoodsmarket:id/recipesSearchResultsGrid']/android.widget.RelativeLayout"));
			
			for (WebElement searchBlock : listOfSearch)
			{
				String searchText = "";
				searchBlock currentBlock = null;
				try
				{
					currentBlock = new searchBlock(driver, searchBlock);
					
				}
				catch (NoSuchElementException e)
				{
					break;
				}
				searchBlocksList.put(currentBlock.getTitle(), currentBlock);
				
			}
			Actions.swipe(driver, 0.5541, 0.8562, 0.5541, 0.6587, 1200);
			System.out.println("Current Count is " + searchBlocksList.size());
		} while (searchBlocksList.size() < expectedCount);
		return searchBlocksList;
	}
	
	public void verifySearchListHasNumberOfResults(int resultsNumberDisplayed)
	{
		Assert.assertEquals(searchBlocksList.size(), resultsNumberDisplayed);
		
	}
	
}