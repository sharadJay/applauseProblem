package com.applause.auto.test;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.applause.auto.framework.test.BaseAppiumTest;
import com.applause.auto.pageframework.pages.homeScreen;
import com.applause.auto.pageframework.pages.searchResultsScreen;
import com.applause.auto.pageframework.testdata.TestConstants;

public class TestSample extends BaseAppiumTest
{
	
	@BeforeMethod(alwaysRun = true)
	public static void testSetup()
	{
	}
	
	@Test(groups = { TestConstants.TestNGGroups.REG }, description = "")
	public void testVerifyNumberOfResultsDisplayed()
	{
		homeScreen hs = new homeScreen(driver);
		searchResultsScreen resultsScreen = hs.enterSearchString("coffee")
												.clickOnSearchButton();
		Assert.assertTrue(resultsScreen.getResultsNumberDisplayed() > 0, "Results are greater than 0");
		resultsScreen.verifySearchListHasNumberOfResults(resultsScreen.getResultsNumberDisplayed());
	}
	
}